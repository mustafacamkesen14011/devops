import uuid
import requests


def random_id(name):
    return "{}_{}".format(name, uuid.uuid4())


def ekranaYaz(response, func_name):
    data = response.json()["results"]
    if len(data):
        for i in data:
            get_value = i["kullanici"]
            user = Kullanici(name=get_value["isim"], email=get_value["mail"])
            print("""
                isim : {}
                mail: {} 
                """.format(user.isim, user.mail))
    else:
        print("kayıt bulunamadı")
    loop()


def kullanıcıDetay():
    email = input("user email : ")
    query = "SELECT * FROM `user` WHERE email='{}' LIMIT 1".format(email)
    response = requests.post("http://192.168.1.9:8093/query/service", data={"statement": query},
                             auth=("Administrator", "database123"))
    ekranaYaz(response, email + " kullanıcı detayı")
    loop()


def tumKullanicilariAl():
    query = "SELECT * FROM `user`"
    response = requests.post("http://192.168.1.9:8093/query/service", data={"statement": query},
                             auth=("Administrator", "database123"))
    ekranaYaz(response, "Tüm kullanıcılar")
    loop()


def yeniKullanıciEkle():
    name = input("name :")
    email = input("email :")

    if name and email:
        query = 'INSERT INTO `user` (KEY,VALUE ) VALUES ("{id}",{name:"{name}",email:"{email}"})'.format(
            id=random_id(name),
            name=name,
            email=email)

        response = requests.post("http://192.168.1.9:8093/query/service", data={"statement": query},
                                 auth=("Administrator", "database123"))

        if response.status_code == 200:
            print("Kullanıcı Başarılı bir şekilde eklendi")
        elif response.status_code == 400:
            print("hat mesajı : ", response.json()["errors"]["msg"])
        else:
            print("Kullancı oluşturulurken bir hata oluştu")
    else:
        print("İsim ve email boş geçilemez")
        yeniKullanıciEkle()
    loop()


def kullaniciSil():
    email = input("email :")
    if email:
        query = 'DELETE FROM `user` WHERE email="{}"'.format(email)
        response = requests.post("http://192.168.1.9:8093/query/service", data={"statement": query},
                                 auth=("Administrator", "database123"))
        if response.status_code == 200:
            print("Kullanıcı Başarılı bir şekilde silindi")
        elif response.status_code == 400:
            print("hat mesajı : ", response.json()["errors"]["msg"])
        else:
            print("Kullancı silinirken bir hata oluştu")
    else:
        print("email boş geçilemez")
        kullaniciSil()
    loop()


def kullanıcıGuncelle():
    email = input("email :")
    new_name = input("yeni isim :")
    new_email = input("yeni email :")
    if email and new_name and new_email:
        query = 'UPDATE `user` SET  name="{}" , email="{}" WHERE email="{}"'.format(new_name, new_email, email)
        response = requests.post("http://192.168.1.9:8093/query/service", data={"statement": query},
                                 auth=("Administrator", "database123"))
        if response.status_code == 200:
            print("Kullanıcı Başarılı bir şekilde güncellendi")
        elif response.status_code == 400:
            print("hat mesajı : ", response.json()["errors"]["msg"])
        else:
            print("Kullancı güncellenirken bir hata oluştu")
    else:
        print("Boş alanlar var lütfen tüm alanları doldurun")
        kullanıcıGuncelle()
    loop()


def loop():
    print("""
    Seçenekler
    1- Tüm kullanıcıları gör
    2- Kullanıcı Detay gör
    3- Yeni Kullanıcı Ekle
    4- Kullanıcı Düzenle
    5- Kullanıcı Silme
    """)

    islemler = {
        1: tumKullanicilariAl,
        2: kullanıcıDetay,
        3: yeniKullanıciEkle,
        4: kullanıcıGuncelle,
        5: kullaniciSil
    }
    try:
        secim = int(input("Seçimiz : "))
        if secim < 1 or secim > 5:
            loop()
        else:
            islemler[secim]()
    except ValueError:
        print("Lütfen geçerli bir seçenek seçiniz")
        loop()


class Kullanici:
    def __init__(self, name, email):
        self.isim = name
        self.mail = email


if __name__ == '__main__':
    loop()
