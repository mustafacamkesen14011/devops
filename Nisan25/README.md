# Docker ile Couchbase Kurulumu ve Konfigürasyonu

![01](./images/1-docker_installation.jpg)

İlk etapta Centos sunucumuza aşağıdaki komut vasıtası ile docker yapısını kuralım.

curl -fsSL https://get.docker.com/ | sh

![01](./images/2-docker_start.jpg)

Sonrasında ise docker hizmetini başlatmak için aşağıdaki komutu koşturalım.
sudo systemctl start docker

![01](./images/3-docker_run_couchbase.jpg)

Sonrasında ise docker vasıtasıyla couchbase sunucumuzu ayağa kaldırmak için aşağıdaki komutu koşturalım.

docker run -d --name db -p 8091-8096:8091-8096 -p 11210-11211:11210-11211 couchbase

![01](./images/5-c1.jpg)

Sonrasında centos sunucumun IP adresini tarayıcıya yazıp 8091 portuna erişim sağlıyorum.
192.168.1.9:8091 

![01](./images/5-c2.jpg)

Bu safhada veritabanı sistemime bir cluster name ve bir kullanıcı hesabı açıyorum.

![01](./images/5-c5.jpg)

Yeni bir Bucket oluşturmak için Bucket sekmesi altına gelip Add Bucket rotasını izleyelim.

![01](./images/5-c6.jpg)

Oluşturacağımız bucket yapısına bir isim verip Add Bucket rotasında devam edelim.

![01](./images/5-c8.jpg)

Query kısmına gelip yeni bir Primary Index oluştururuz. Bunun için aşağıdaki komutu koşturmamız yeterli olacaktır. 

CREATE PRIMARY INDEX ON `dbtrendyol`

![01](./images/5-c9.jpg)

Bucket yapısı içerisine key ve value değerleri belli olan anahtar ve kilit ifadelerini json dosya formatına couchbase ile göndermek için aşağıdaki komutu koşturmamız gereklidir.

INSERT INTO `dbtrendyol` (KEY,VALUE) VALUES (“anahtar”,”kilit”)

![01](./images/5-c10.jpg)

Bu işlemin kontrolünü ise dbtrendyol bucketi içinden aşağıdaki komutu koşturarak doğrulayabiliriz.

SELECT * FROM `dbtrendyol`

![01](./images/6-postman.jpg)

Aynı şekilde POSTMAN aracı ile bir HTTP POST request mesajı ile doğrulamamızı yapmış oluruz.

# Case2 Python Backend Modülü

Python backend modülü main.py dosyasıdır.



