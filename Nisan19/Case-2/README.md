# 2. Case

Öncelik olarak buradaki senaryomda Ansible otomasyon aracıyla, veritabanı sistemini kullanan Wordpress altyapısının Nginx üzerinden dockerized edilip yayına alınması adımları incelenmektedir.

NOTLAR:
Senaryoda 2 adet Ubuntu 18.04.5 sürümlü işletim sistemi vardır. Ana sanal işletim sisteminde Ansible kuruludur. Diğer işletim sistemimde ise sadece bağlantı için hazırlanmış SSH konfigürasyonlarından başka hiçbir yapılandırma ve araç kurulu değildir. Buradaki yapıda Ana sanal işletim sisteminden Ansible ile karşıdaki sunucunun otomatize edilmesi amaçlanmıştır.

# 2.1. DB kullanan bir uygulamayı dockerized edip, nginx üzerinden serve edilmesi 
    • DB kullanan bir uygulamanın ansible ile dockerize edilmesi 
    • Docker kurulumu ve container ayaga kaldırma islemlerinin ansible ile yapılması


![01](./images/1-ansible%20kurulum.jpg)    

Öncelik olarak ana makinede ansible kurulumlarını yapmak adına aşağıdaki komutu koşturmamız gereklidir.

```bash
sudo apt install software-properties-common && sudo apt-add-repository --yes --update ppa:ansible/ansible && sudo apt install ansible
```

![01](./images/2-ansible%20versiyon.jpg)  

Kurulumu tamamladıktan sonra aşağıdaki komutu koşturarak 2.9.20 sürümünün sağlıklı olarak kurulduğunu teyit etmiş oluruz.

```bash 
ansible --version
```
 

![01](./images/2-ubuntuclient-ssh%20server-install.jpg)

Kurguladığımız yapının karşı sunucuda servis edilebilmesi için öncelikle ubuntu paket repolarını güncelledikten sonra openssh servisini kurmamız yeterli olacaktır. Bunun için aşağıdaki komutu koşturmalıyız.

```bash 
sudo apt update
```
```bash
sudo apt install openssh-server
```

![01](./images/3-client%20check%20ping%20pong%20ansible%20command.jpg)

Ana makinemizden karşı taraftaki makinenin ayakta olduğunu anlamak için ansible dizini içinden aşağıdaki komutu koşturarak iletişim doğrulamasını yapalım.
```bash
ansible all -m ping -u mustafa --ask-pass
```
![01](./images/5-ansible%20task%20run%20v2.jpg)

Akabinde bizim için gerekli olan ansible playbookları içerisindeki taskların otomatik kurulumları ve karşı sunucuda bu hizmetlerin servisi için aşağıdaki komut koşturulur.
```bash
ansible-playbook -u mustafa -i hosts --ask-pass --ask-become-pass -T 60 wordpress.docker.yml
```
![01](./images/6-final.jpg)

İşlemlerimiz sağlıklı bir şekilde bittiğinde alacağımız çıktı bu şekilde olacaktır.

![01](./images/7-poc.jpg)

![01](./images/6-poc.jpg)

Bize belirtilen statik sayfamızda Postman aracı ile istenilen header değerleri olarak bootcamp=devops ayarladığımızda 192.168.1.8 IP adresli servis sunucumuza HTTP GET isteği gönderildikten sonra sayfamızın Hoş geldin Devops sayfasına yönlendirildiği ispatlanmıştır.

