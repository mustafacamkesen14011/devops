#  1.1	Sanal makinada bir Centos kurup, güncellemelerinin yapılması

Öncelik olarak benim kullanacağım bu senaryoda VMware Workstation 16 Pro sanallaştırma hipervizörünün 16.1.1 versiyonu üzerine Centos kurulumları gerçekleştirilecektir.
İlk iş olarak VMware Workstation uygulamasında sanal bir VM (virtual machine – sanal makine) oluşturalım.

![01](./images/1.jpg)

VMware Workstation 16 Uygulamasında karşımıza çıkan ilk ekranda “Create a New Virtual Machine” rotasını takip ederek sanal bir makine oluşturma işlemlerimizin ilk adımından başlayalım.

![01](./images/2.jpg)

Bu adımda tercihinize göre varsayılan ayarlarda devam edebilirsiniz. Lakin ben Custom rotasında devam edip detaylı bir sanal makine oluşturma tercihini seçerek tüm adımlarımı ayrıntılı ve tek seferde oluşturmayı tercih ettim.

![01](./images/3.jpg)

Karşımıza çıkan bu yönergede ise halihazırda Workstation 16 sürümünü kullandığım için varsayılan rotada devam ediyorum.

![01](./images/4.jpg)

İşletim sistemimizin imaj dosyasını seçip “Next” rotasında devam edebiliriz.

![01](./images/5.jpg)

Sonrasında sanal makinemize bir isim ve sanal makinemizin kurulacağı local disk yolunu belirledikten sonra “Next” rotasında devam edebiliriz.

![01](./images/6.jpg)

Daha sonra işletim sistemimize ayrılacak 4 core CPU ( Central Processing Unit – İşlemci) tahsisini yaptıktan sonra “Next” rotasında devam edebiliriz.

![01](./images/7.jpg)

Akabinde işletim sistemimize ayrılacak 4GB bellek miktarını belirledikten sonra “Next” rotasında devam edebiliriz.

![01](./images/8.jpg)

Sonrasında işletim sistemimizin ağ bağlantı ayarlamaları ekranında ise tercihinize göre bağlantı türünü belirleyebilirsiniz. Ben buradaki örneğimde “Bridge Mode” seçeneğini seçerek işletim sistemimin direk LAN ağına bağlanmasını aynı zamanda ise hop sayısını arttırmaktan kaçınarak bu bağlantı türünü tercih ettim.

![01](./images/9.jpg)
![01](./images/9.1.jpg)

Karşımıza çıkan ekranda ise disk türü seçimlerimizi ve ayarlamalarını yaptıktan sonra “Next” rotasında devam edebiliriz.

![01](./images/10.jpg)

Burada ise sanal diskimizi oluşturup “Next” rotasında ilerleriz.

![01](./images/11.jpg)

Buradaki işlemimizde ise oluşturmuş olduğumuz sanal diskin boyutlandırmasını yaptıktan sonra “Next” rotasında devam edebiliriz.

![01](./images/12.jpg)

Disk konfigürasyonlarımızı yaptıktan sonra “Next” rotasında ilerleyebiliriz.

![01](./images/13.jpg)

Sanal makine oluşturma işlemlerini sonlandırıp işletim sistemi kurulumu adımına geçmek için ise “Finish” ile işlemlerimizi sonlandıralım.

![01](./images/14.jpg)

Karşımıza çıkan kurulum ekranında “Install Centos 7” rotasında devam edelim.

![01](./images/15.jpg)

Dil ayarlarımızı seçtikten sonra “Continue” rotasında devam edebiliriz.

![01](./images/16.jpg)

Bu kurulum ekranında ise işletim sistemimizin kurulacağı hedef diski tanıtmamız gerekmektedir.

![01](./images/17.jpg)

Akabinde işletim sisteminin kurulacağı hedef diski tanımlarız.

![01](./images/18.jpg)

Kurulum diski tanımlamasını yaptıktan sonra “Begin Installation” ile kurulumu başlatabiliriz.

![01](./images/19.1.jpg)

Arka planda kurulum işlemleri devam ederken Root kullanıcısı için bir parola belirleme işlemine geçelim

![01](./images/19.jpg)

Parolamızı belirledikten sonra bu işlemi de tamamlamış oluruz.

![01](./images/20.jpg)

Son olarak kurulum adımları tamamlanmıştır. Şimdi sistemi yeniden başlatmak için “Reboot” rotasını izleyelim.

![01](./images/21.jpg)

Görmüş olduğunuz gibi işletim sistemimiz kullanıma hazırdır. Lakin Centos 7 de repo güncellemelerini yapmak için öncelikle internete bağlı olmamız gerekmektedir.
Centos 7 işletim sisteminde default olarak NIC (Network Interface Card – Ağ Arayüz Kartı) birimleri kapalı olarak gelmektedir. 
Şimdiki adımlarımızda ise NIC birimini aktifleştirelim ve bu birime statik olarak bir IP adresi tanımlayalım.


![01](./images/part2/1.jpg)

Ağ ayarlarını yapmak için nmtui komutunu koşturalım ve Centos’ un ağ yönetim arayüzüne geçelim.

![01](./images/part2/2.jpg)

Öncelik olarak “Activate a Connection” rotasını izleyerek NIC birimini aktifleştirelim.

![01](./images/part2/3.jpg)

Enter ile NIC birimini aktif etmeyi gerçekleştirelim.

![01](./images/part2/4.jpg)

Akabinde ise statik IP adresi tanımlama işlemlerine geçmek için “Edit a connection” rotasını izleyelim.

![01](./images/part2/5.jpg)

Arayüzü düzenlemek için “Edit” rotasını izleyelim.

![01](./images/part2/6.jpg)

IP v4 konfigürasyonunu “Manuel” olarak ayarlayalım ve “Show” rotasında devam edelim.

![01](./images/part2/7.jpg)

Daha sonrasında ise IP adresi, default gateway, DNS gibi parametrelerimizi manuel olarak tanımlayalım.

![01](./images/part2/8.jpg)

Gerekli olan parametrelerimizi tanımladıktan sonra “Ok” rotasında devam edebiliriz.

![01](./images/part2/9.jpg)

Son olarak ağ yönetim arayüzünde ki işlemlerimizi sonlandırıp bu menüden çıkabiliriz.

![01](./images/part2/10.jpg)

Sistemimizde henüz net-tools paketleri kurulu olmadığı için ifconfig komutunu koşturduğumuzda komutumuz çalışmayacaktır. Şimdilik ip address komutunu koşturarak gerekli olan doğrulama işlemlerimizi yapabiliriz. Ben bu adımdan sonra arka planda yum install net-tools -y komutumu koşturarak net-tools paketimi yükledim.

![01](./images/part2/11.jpg)

İşletim sistemimizde repolarımızı güncellemek için “yum update -y” komutu vasıtası ile repolarımızı güncelleme işlemlerini gerçekleştirelim.

![01](./images/part2/12.jpg)

Sonuç olarak artık repolarımızı başarılı bir şekilde güncellediğimizi görmekteyiz.

# 1.2 Kişisel bir user yaratılması (ad.soyad şeklinde) Not: aşağıdaki işlemler bu kullanıcı ile yapılacak

![01](./images/part2/13.jpg)

adduser mustafa.camkesen komutunu koşturarak öncelikle yeni bir kullanıcı işlemini tanımladıktan sonra passwd mustafa.camkesen komutunu koşturarak kullanıcımıza bir parola ataması işlemini bu işlemle yapmış bulunuyoruz.

![01](./images/part2/14.jpg)
![01](./images/part2/14.1.jpg)

İleride gerçekleştireceğimiz birtakım komut koşturmaları ve işlemlerde yetki kısıtlamalarına takılmamak adına mustafa.camkesen isimli kullanıcıya sudoers dosyasında yetkilendirme işlemleri için visudo komutunu koşturarak bu doyaya mustafa.camkesen ALL=(ALL) ALL komutu ile yetkilerini verip işlemlerimizi Escape tuşuna bastıktan sonra :wq ile kaydedip çıkabiliriz.

![01](./images/part2/15.jpg)

Sonrasında ise oluşturduğumuz kullanıcı hesabına geçiş yapabildiğimizi görmekteyiz.

# 1.3. Sunucuya 2. disk olarak 10 GB disk eklenmesi ve "bootcamp" olarak mount edilmesi

![01](./images/part3/1-lsblk.jpg)

İlk olarak varolan diskleri görüntülemek için lsblk komutunu koşturalım. Bu adımı görüntüledikten sonra disk ekleme işlemlerimize geçebiliriz.

![01](./images/part3/2-diskekleme-vm1-yedek.jpg)

Sanal makinemizi kapattıktan sonra sisteme yeni bir disk dahil etmek için “Edit virtual machine Settings” rotasında ilerleyelim.

![01](./images/part3/3.jpg)

Akabinde “Add” rotasını izleyelim.

![01](./images/part3/4.jpg)

Sisteme eklemek istediğimiz birimi seçtikten sonra “Next” rotasında ilerleyelim.

![01](./images/part3/5.jpg)

Disk tipini belirledikten sonra “Next” rotasında ilerleyelim.

![01](./images/part3/6.jpg)

Yeni bir disk oluşturmak için “Create a new virtual disk” seçimi ile “Next” rotasını izleyelim.

![01](./images/part3/7.jpg)

Disk boyutumuzu seçelim. Ekran görüntüsünde 10GB görünmektedir lakin ben bir aksilik olması durumunda bu boyutu 20GB olarak belirledim işletim sistemime ise bu diskin 10GB kadarını mount ettim. Bu adımdan sonra “Next” rotasını takip edelim.

![01](./images/part3/8.jpg)

İşlemlerimizi sonlandırmak adına “Finish” rotasını takip edelim.

![01](./images/part3/11.jpg)

Sanal makinemizi başlattıktan sonra df -h komutunu koşturarak disk miktarlarını ve kullanımlarını listeleyebiliriz. Burada yeni eklediğimiz disk henüz görünmemektedir. Bunun için ise mount işlemleri gerekmektedir.

![01](./images/part3/12.jpg)

Disk mount işlemlerinin ilk adımında fdisk ile işlemlerimize başlayalım. Bunun için sudo fdisk /dev/sdb komutunu koşturalım. Ardından yardım menüsüne erişmek için m parametresini koşturalım.

![01](./images/part3/13.jpg)

Akabinde ise yeni bir partition eklemek için n parametresini koşturalım. Biz burada primary partition seçeceğimiz için default değerleri seçelim ve mount etmek istediğimiz 10GB disk miktarını belirleyelim. Bu işlemi yaptıktan sonra ise w parametresini koşturarak işlemlerimizi kaydedip çıkabiliriz.

![01](./images/part3/15.jpg)

Eklediğimiz diskin dosya sistemini belirlemek adına sudo mkfs.ext4 /dev/sdb1 komutunu koşturarak dosya sistemimizi belirleyelim

![01](./images/par4/1-düzeltme.jpg)

Diskimizi mount edecek bir alan oluşturmak maksadı ile root dizini altında olmayan bir bootcamp dizini oluşturmak için sudo mkdir -p /bootcamp komutu ile diskimizi mount edecek dizini oluşturalım.

![01](./images/par4/2-düzeltme.jpg)

Diskimizin mount işlemi için /dev/sdb1 dizininde belirtilen diski önceden /bootcamp altında olan dizine mount işlemlerini gerçekleştirelim. Bunun için sudo mount /dev/sdb1 /bootcamp komutunu koşturalım.

![01](./images/part3/18.jpg)

Tüm bu işlemlerimizin kalıcı ve garanti olmasını sağlamak maksadıyla fstab dosyasında mount edeceğimiz diski ve mount edilecek dizini bu dosya altına işlemek için sudo vi /etc/fstab komutunu koşturalım.

![01](./images/par4/3-düzeltme.jpg)

Dosyamızın içine girdikten sonra diskimizi /bootcamp olarak dosya sistemini ext4 ve dosya okuma yazma işlemleri için default ayarlamalarını yaptıktan sonra yaptığımız değişiklikleri kaydedip çıkıyoruz.

![01](./images/part3/20.jpg)

Fstab dosyasındaki tanımlamaların otomatik olarak mount edilmesi için son olarak sudo mount -a komutunu koşturarak disk mount işlemlerimizi tamamen sonlandırmış oluyoruz.

![01](./images/par4/4-son düzeltme.jpg)


Görüldüğü üzere diskimiz doğru bir şekilde ve doğru yere mount edildiği gözlemlenmektedir.

# 1.4. /opt/bootcamp/ altında bootcamp.txt diye bir file yaratılıp, file'ın içerisine "merhaba trendyol" yazılması

![01](./images/par4/6-düzeltme.jpg)

Öncelik olarak sudo mkdir -p /opt/bootcamp dizinini oluşturalım. Burada -r parametresi dizin içinde dizin oluşturabilmek için kullandığımız bir parametredir. Akabinde oluşturduğumuz dize cd  komutu ile geçiş yapalım. Daha sonrasında sudo touch bootcamp.txt komutunu koşturarak bizden istenen metin dosyasını oluşturalım ve dosya yetki verme işlemlerini chmod komutu ile gerçekleştirelim. Ben buradaki 777 izin politikasında dosyanın herkes tarafından okunabilmesi, yazılabilmesi ve çalıştırabilmesi yetkisini verdim. Sonrasında ise dosyamızı düzenlemek için vi editörüyle dosyamızı açalım ve düzenleme işlemlerini yapalım.

![01](./images/par4/7-düzeltme.jpg)

Akabinde vi editörü ile dosyamıza “merhaba trendyol” içeriğini ekleyelim.

![01](./images/par4/8-düzeltme.jpg)

Son olarak cat komutu vasıtasıyla dosyamızı okuyalım.

# 1.5. Kişisel kullanıcının home dizininde tek bir komut kullanarak bootcamp.txt file'ını bulup, bootcamp diskine taşınması

![01](./images/case1final.jpg)

Son olarak ise aramak istedim bootcamp.txt dosyasını doğrudan root dizininde ( /opt/bootcamp altında olduğunu bilmiyorsam) sudo find / -name 'bootcamp.txt' -exec mv -t /bootcamp/ {} + komutunu koşturarak işlemimizi tamamlamış oluruz












